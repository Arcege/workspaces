if [[ $- == *i* ]]; then   # only for interactive
    _ws_complete () {
        # handle bash completion
        local options commands names
        options="-h --help"
        commands="config convert create current debug destroy enter help hook"
        commands="$commands initialize leave list logging plugin relink reload"
        commands="$commands stack release state upgrade use validate version versions"
        names=$(ws list -q | _ws_tr '\n' ' ')
        COMPREPLY=()
        #compopt +o default  # not available on Darwin version of bash
        if [ $COMP_CWORD -eq 1 ]; then
            COMPREPLY=( $(compgen -W "$commands $options $names" -- ${COMP_WORDS[COMP_CWORD]}) )
            return 0
        else
            local i=2 cmd cur curop prev state
            cur="${COMP_WORDS[COMP_CWORD]}"
            curop="${COMP_WORDS[1]}"
            prev="${COMP_WORDS[COMP_CWORD-1]}"
            case ${curop} in
                help)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "$commands" -- ${cur}) )
                    fi
                    ;;
                leave|current|state|version|validate)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "-h --help" -- ${cur}) )
                    fi
                    ;;
                enter)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "-h --help $names" -- $cur) )
                    else
                        local configvars=$(_ws_show_all_config_vars)
                        COMPREPLY=( $(compgen -W "${configvars}" -f -- ${cur}) )
                    fi
                    ;;
                destroy|relink)
                    if [ $COMP_CWORD -eq 2 ]; then
                        if [ -n "$_ws__current" ]; then
                            COMPREPLY=( $(compgen -W "- -h --help $names" -- $cur) )
                        else
                            COMPREPLY=( $(compgen -W "-h --help $names" -- $cur) )
                        fi
                    fi
                    ;;
                debug|logging)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "-h --help reset show 0 1 2 3 4 5 6 8 9" -f -- ${cur}) )
                    fi
                    ;;
                list)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "-q --quiet" -- ${cur}) )
                    fi
                    ;;
                reload)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -f -W "-h --help" -- ${cur}) )
                    fi
                    ;;
                use)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -d -- ${cur}) )
                    elif [ $COMP_CWORD -eq 3 ]; then
                        # this gets around some quoting issues in bash
                        local wsdir=$(builtin eval builtin echo ${COMP_WORDS[2]})
                        names=$(WS_DIR=$wsdir ws list -q)
                        COMPREPLY=( $(compgen -W "$names" -- ${cur}) )
                    fi
                    ;;
                initialize)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -d -- ${cur}) )
                    fi
                    ;;
                upgrade)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "-h --help --dry-run $(_ws_get_versions)" -- ${cur}) )
                    elif [ $COMP_CWORD -gt 2 ]; then
                        COMPREPLY=( $(compgen -W "$(_ws_get_versions)" -- ${cur}) )
                    fi
                    ;;
                release)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "--full -h --help $names" -- ${cur}) )
                    elif [ $COMP_CWORD -eq 3 -a "x${prev}" = x--full]; then
                        COMPREPLY=( $(compgen -W "${names}" -- ${cur}) )
                    fi
                    ;;
                stack)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "help del" -- ${cur}) )
                    elif [ $COMP_CWORD -gt 2 -a x${prev} = xdel ]; then
                        COMPREPLY=( $(compgen -W  "$names" -- ${cur}) )
                    fi
                    ;;
                versions)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "clear $(_ws_get_versions)" -- ${cur}) )
                    fi
                    ;;
                hook)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "copy edit help load run save -h --help" -- ${cur}) )
                    elif [ $COMP_CWORD -eq 3 -a \
                           \( "x${prev}" = xedit -o "x${prev}" = xcopy -o \
                              "x${prev}" = xload -o "x${prev}" = xsave \) ]; then
                        if [ -n "$_ws__current" ]; then
                            COMPREPLY=( $(compgen -W "$names - --global --skel" -- ${cur}) )
                        else
                            COMPREPLY=( $(compgen -W "$names --global --skel" -- ${cur}) )
                        fi
                    elif [ $COMP_CWORD -eq 4 -a "x${COMP_WORDS[2]}" = xcopy ]; then
                        if [ -n "$_ws__current" ]; then
                            COMPREPLY=( $(compgen -W "$names -" -- ${cur}) )
                        else
                            COMPREPLY=( $(compgen -W "$names" -- ${cur}) )
                        fi
                    elif [ $COMP_CWORD -eq 4 -a "x${COMP_WORDS[2]}" = xload ]; then
                        COMPREPLY=( $(compgen -f -- ${cur}) )
                    fi
                    ;;
                create)
                    state=name  # one of 'plugins', 'name', 'cfg'
                    while [ $i -lt $COMP_CWORD ]; do
                        case $state in
                            name)
                                case ${COMP_WORDS[i]} in
                                    -p|--plugins) state=plugins;;
                                    *) state=cfg;;
                                esac
                                ;;
                            plugins) state=name ;;
                            cfg) : ;;  # no state change
                        esac
                        let i++
                    done
                    case $state in
                        plugins)
                            COMPREPLY=( $(compgen -W "ALL $(_ws_cmd_plugin available)" - ${cur}) )
                            ;;
                        name)
                            COMPREPLY=( $(compgen -W "-h --help -p --plugins" -- ${cur}) )
                            ;;
                        cfg)
                            local configvars pluginvars
                            configvars=$(_ws_show_all_config_vars)
                            pluginvars=$(_ws_cmd_plugin show -q)
                            COMPREPLY=( $(compgen -W "${configvars} ${pluginvars}" -- ${cur}) )
                            ;;
                    esac
                    ;;
                config)
                    # one of 'cmd', 'name', 'var', 'val', 'verb', 'file', 're' or 'end'
                    state=cmd
                    while [ $i -lt $COMP_CWORD ]; do
                        case $state in
                            val) state=end;;
                            var)
                                case $cmd in
                                    set) state=val;;
                                    *) state=end;;
                                esac
                                ;;
                            name)
                                case $cmd in
                                    -h|--help) state=end;;
                                    list) state=verb;;
                                    load) state=file;;
                                    set|get|del) state=var;;
                                    *) state=end
                                esac
                                ;;
                            cmd)
                                cmd=${COMP_WORDS[i]}
                                case $cmd in
                                    search) state=re;;
                                    *) state=name;;
                                esac
                                ;;
                            *) state=end;;
                        esac
                        if [ $state = end ]; then
                            break
                        fi
                        let i++
                    done
                    case $state in
                        verb)
                            COMPREPLY=( $(compgen -W " -v --verbose" -- ${cur}) )
                            ;;
                        cmd)
                            COMPREPLY=( $(compgen -W "-h --help del get help list load search set" -- ${cur}) )
                            ;;
                        name)
                            if [ -n "$_ws__current" ]; then
                                COMPREPLY=( $(compgen -W "- --global ${names}" -- ${cur}) )
                            else
                                COMPREPLY=( $(compgen -W "--global ${names}" -- ${cur}) )
                            fi
                            ;;
                        file)
                            COMPREPLY=( $(compgen -f -- ${cur}) )
                            ;;
                        var|re)
                            local configvars pluginvars
                            configvars=$(_ws_show_all_config_vars)
                            pluginvars=$(_ws_cmd_plugin show -q)
                            COMPREPLY=( $(compgen -W "$configvars $pluginvars" -- ${cur}) )
                            ;;
                    esac
                    ;;
                plugin)
                    state=cmd  # one of 'cmd', 'install', 'name', 'wsname', 'file', 'end', "quiet"
                    while [ $i -lt $COMP_CWORD ]; do
                        case $state in
                            file|quiet) state=end;;
                            cmd)
                                cmd=${COMP_WORDS[i]}
                                case $cmd in
                                    available|-h|--help) state=end;;
                                    install) state=install;;
                                    uninstall) state=name;;
                                    list|add|remove) state=wsname;;
                                    show) state=quiet;
                                esac
                                ;;
                            install)
                                case ${COMP_WORDS[i]} in
                                    -f) : ;;  # no state change
                                    -n) state=name;;
                                    *) state=file;;
                                esac
                                ;;
                            name)
                                case $cmd in
                                    install) state=file;;
                                    uninstall) state=end;;
                                    add|remove) : ;;  # no state change
                                esac
                                ;;
                            wsname)
                                case $cmd in
                                    list) state=end;;
                                    *) state=name;;
                                esac
                                ;;
                        esac
                        if [ $state = end ]; then
                            break
                        fi
                        let i++
                    done
                    case $state in
                        cmd)
                            COMPREPLY=( $(compgen -W "-h --help available install uninstall list add remove show" -- ${cur}) )
                            ;;
                        install)
                            COMPREPLY=( $(compgen -f -W "-f -n" -- ${cur}) )
                            ;;
                        name)
                            COMPREPLY=( $(compgen -W "$(_ws_cmd_plugin available)" - ${cur}) )
                            ;;
                        file)
                            COMPREPLY=( $(compgen -f -- ${cur}) )
                            ;;
                        wsname)
                            if [ -n "$_ws__current" ]; then
                                COMPREPLY=( $(compgen -W "$names" -- ${cur}) )
                            else
                                COMPREPLY=( $(compgen -W "- $names" -- ${cur}) )
                            fi
                            ;;
                        quiet)
                            COMPREPLY=( $(compgen -W "-q" -- ${cur}) )
                            ;;
                    esac
                    ;;
                convert)
                    state=dir  # one of 'dir', 'name', 'plugins', 'cfg'
                    while [ $i -lt $COMP_CWORD ]; do
                        case $state in
                            dir)
                                case $cur in
                                    -n|--name) state=name;;
                                    -p|--plugins) state=plugins;;
                                    *) state=cfg;;
                                esac
                                ;;
                            name)
                                state=dir
                                ;;
                            plugins)
                                state=dir
                                ;;
                            cfg) : ;;  # no state change
                        esac
                        let i++
                    done
                    case $state in
                        # no completion for state=name
                        dir)
                            COMPREPLY=( $(compgen -d -W "-h --help -n --name -p --plugins" -- ${cur}) )
                            ;;
                        plugins)
                            COMPREPLY=( $(compgen -W "$(_ws_cmd_plugin available)" - ${cur}) )
                            ;;
                        cfg)
                            local configvars pluginvars
                            configvars=$(_ws_show_all_config_vars)
                            pluginvars=$(_ws_cmd_plugin show -q)
                            COMPREPLY=( $(compgen -W "$configvars $pluginvars" -- ${cur}) )
                            ;;
                    esac
                    ;;
                *)
                    if [ $COMP_CWORD -eq 2 ]; then
                        COMPREPLY=( $(compgen -W "-h --help" -- ${cur}) )
                    fi
                    ;;
            esac
        fi
        return 0
    }

    # activate bash completion
    complete -F _ws_complete ws
fi


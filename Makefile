# Makefile for workspaces
# # Copyright @ 2020 Michael P. Reilly. All rights reserved

MKDIR := mkdir -p
CP := cp
LN := ln
SED := sed

RELEASE_FILES := \
	install.sh \
	LICENSE.txt \
	README.md \
	ws.sh \
	wsh.sh \
	completion_bash.sh \
	completion_zsh.sh \
	version \
	plugins/ \
	test/ \

TO_TOKENIZE = \
	$(BUILD_DIR)/workspaces/ws.sh \
	$(BUILD_DIR)/workspaces/test/common.sh

BUILD_DIR := $(PWD)/build
DIST_DIR := $(PWD)/dist

TARFILE = $(DIST_DIR)/workspaces.tgz
VERSION := $(shell cat version)

USER := Arcege

BASEURL := https://api.bitbucket.org/2.0/repositories/$(USER)/$(BITBUCKET_REPO_SLUG)/downloads

ifeq (,$(BITBUCKET_REPO_SLUG))
    BITBUCKET_REPO_SLUG := workspaces
endif

ifeq (,$(BITBUCKET_TAG))
ifeq (,$(BITBUCKET_BUILD_NUMBER))
	VERS := SNAPSHOT
else
	VERS := b$(BITBUCKET_BUILD_NUMBER)
endif
else
	VERS := $(BITBUCKET_TAG)
endif

RELEASE_TAR = $(BITBUCKET_REPO_SLUG)-$(VERS).tgz

all: clean build test package

package: build $(TARFILE)

test: build
	./test/suite.sh $(BUILD_DIR)/workspaces

testfull: build
	./test/suite.sh --full $(BUILD_DIR)/workspaces

build: compile

compile: init $(BUILD_DIR)/workspaces

$(BUILD_DIR)/workspaces: $(BUILD_DIR)/.tstamp
	$(CP) -a $(RELEASE_FILES) $(BUILD_DIR)/workspaces
	$(SED) -i -e "s/SNAPSHOT/$(VERSION)/" $(TO_TOKENIZE)

$(BUILD_DIR)/.tstamp: $(RELEASE_FILES)
	touch $@

release: tag push

init:
	$(MKDIR) $(BUILD_DIR)/workspaces $(DIST_DIR)

tag: check-tag
	git tag "$(shell cat version)" HEAD
	rm .tag-cache

push:
	git push --tags

check-tag: .version-regex .tag-cache
	! grep -qsf .version-regex .tag-cache

.tag-cache:
	git fetch --tags
	git tag > $@

.version-regex: version
	sed 's/.*/^&$$/' $< > $@

$(TARFILE): $(DIST_DIR)/.tstamp

$(DIST_DIR)/.tstamp: $(BUILD_DIR)/.tstamp
	tar czfC $(TARFILE) $(BUILD_DIR) workspaces
	touch $@

$(DIST_DIR)/$(RELEASE_TAR): $(TARFILE)
	$(LN) -f $< $@

isnt_uploaded:
	! curl -s --user $(BB_AUTH_STRING) $(BASEURL) | jq '.values[].name' | grep -qs $(RELEASE_TAR)

upload: isnt_uploaded init $(DIST_DIR)/$(RELEASE_TAR)
	cd $(DIST_DIR) && curl -X POST --user $(BB_AUTH_STRING) $(BASEURL) --form files=@$(RELEASE_TAR)

clean:
	$(RM) -r $(BUILD_DIR) $(DIST_DIR)
	$(RM) .version-regex .tag-cache

cleantest: clean
	$(RM) test.err test.log

.PHONY: test

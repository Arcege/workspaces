if [[ $- == *i* ]]; then   # only for interactive
    # for now use bash compatibility
    autoload bashcompinit && bashcompinit
    if [ $? -eq 0 ]; then
        source ${_WS_INSTALLDIR}/completion_bash.sh
    fi
fi
